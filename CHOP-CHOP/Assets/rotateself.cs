﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateself : MonoBehaviour
{
    Vector2 oldMouse = new Vector2(1, 0), newMouse = new Vector2(1, 0);
    // Use this for initialization
    void Start()
    {

        //Debug.Log(Vector2.SignedAngle(oldMouse, newMouse));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            this.transform.position = Input.mousePosition;
            newMouse = Input.mousePosition;
            if ((newMouse - oldMouse).magnitude >= 1)
            {
                //Debug.Log(Vector2.SignedAngle(new Vector2(1, 0), newMouse - oldMouse));
                Vector3 rotationVector3 = new Vector3(0f, 0f, Vector2.SignedAngle(new Vector2(1, 0), newMouse - oldMouse) - 90f);
                transform.rotation = Quaternion.Euler(rotationVector3);
                oldMouse = newMouse;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            
            this.transform.position = new Vector3(0, 10000, 0);
        }
    }
}
