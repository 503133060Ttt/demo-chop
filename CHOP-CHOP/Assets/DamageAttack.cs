﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAttack : MonoBehaviour {
    // Use this for initialization
    public Transform[] mSwordHitablePoints = new Transform[3];
    void Start () {

    }

    private void Update()
    {
        RaycastHit hinfo;
        //int mask = 1 << LayerMask.NameToLayer("Defaut");// 只取与FoeHitReceiver层相交
        GameObject hitObj = null;
        if (Physics.Linecast(mSwordHitablePoints[0].position, mSwordHitablePoints[1].position, out hinfo))
        {
            hitObj = hinfo.collider.gameObject;
        }
        else if (Physics.Linecast(mSwordHitablePoints[1].position, mSwordHitablePoints[2].position, out hinfo))
        {
            hitObj = hinfo.collider.gameObject;
        }
        if (hitObj != null && hitObj.tag == "Player")
        {
            //Debug.LogError("hitObj.name" + hitObj.name + "name" + GameObject.FindGameObjectWithTag("Player"));
            PhotonView pv = hitObj.transform.GetComponent<PhotonView>();
            if (hitObj.name != GameObject.FindGameObjectWithTag("Player").name)
            {
                pv.RPC("HPDamage", PhotonTargets.All, 50);
                if (hitObj.transform.GetComponent<PlayerINFO>().HP > 0)
                {
                    pv.RPC("PlayMusic", PhotonTargets.All, 1);
                }
                pv.RPC("Penxue", PhotonTargets.All);
                if (hitObj.transform.GetComponent<PlayerINFO>().HP == 0)
                {
                    PhotonView pvSelf = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<PhotonView>();
                    pvSelf.RPC("Killadd", PhotonTargets.All, 1);
                    pv.RPC("Deathadd", PhotonTargets.All);
                }
            }
        }
    }
}
    
