﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.AI;
using PigeonCoopToolkit.Effects.Trails;

public class LineManager : MonoBehaviour
{
    public LayerMask mask;
    public LayerMask mask2 = 9;
    public List<Vector3> L3;
    public List<Vector3> L4;
    public bool isMove = false;
    public Transform[] mSwordHitablePoints = new Transform[6];
    public bool isBack = false;
    public Image imageBack;
    public Text textBack;
    public GameObject playerSelf;
    private float old_y = 0;
    private int clickLength = 150;
    public GameObject arr;
    //记录当前的重力感应的Y值
    private float new_y;
    //当前手机晃动的距离
    private float currentDistance = 0;
    public bool isXY = false;
    //手机晃动的有效距离
    private float distance = 0.4f;

    public float backTran = 2;
    private NavMeshAgent navMeshAgent;
    public GameObject imageMouse;
    public GameObject imageMouse2;
    public GameObject imageMouse3;
    private Vector2 oldMouse = new Vector2(1,0), newMouse = new Vector2(1, 0);
    private bool getKill = false;
    public GameObject targetPlayer;
    // Use this for initialization
    void Start()
    {
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        playerSelf = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(refreshBackTran());
        imageMouse = GameObject.FindGameObjectWithTag("Mouse");
        if(playerSelf.GetComponent<PlayerINFO>().zhiYe == 2)
        {
            imageMouse2 = GameObject.FindGameObjectWithTag("Mouse");
            imageMouse3 = GameObject.FindGameObjectWithTag("Mouse2");
        }
    }
    [PunRPC]
    /*void SetTargetPlayer(string gb)
    {
        targetPlayer = GameObject.Find(gb);
        Debug.Log("targetPlayer" + targetPlayer);*/
       /* if (targetPlayer != null)
        {
            Debug.Log(targetPlayer.name);
            Vector3 rotationVector3 = new Vector3(0f, 0f, Vector2.SignedAngle(new Vector2(1, 0), new Vector2(targetPlayer.transform.position.x - transform.position.x,
            targetPlayer.transform.position.z - transform.position.z)) - 45f);
            arr.transform.rotation = Quaternion.Euler(rotationVector3);

        }}*/

    IEnumerator refreshBackTran()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            {
                imageBack.GetComponent<Image>().fillAmount = (backTran - (int)backTran);
                if (backTran < 3)
                    backTran = backTran + 0.01f;
            }
        }
    }

    // Update is called once per frame
    IEnumerator BackRotate()
    {
        yield return new WaitForSeconds(0.2f);
        isBack = false;
    }
    [PunRPC]
    void SmokeStart()
    {
        if(GetComponent<PlayerINFO>().place == 0)
        {
            transform.GetChild(1).position = transform.position + new Vector3(0, 100, 0);
        }
        else
        {
            transform.GetChild(1).position = transform.position - new Vector3(0, 100, 0);
        }
        transform.GetChild(1).position = transform.position;

        StartCoroutine(SmokeRemove());
    }
    IEnumerator SmokeRemove()
    {
        yield return new WaitForSeconds(3f);
        transform.GetChild(1).position = transform.position + new Vector3(0, 1000, 0);
    }
    IEnumerator CheckCamera()
    {
        //Debug.Log("12333333321   " + GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFilterPack_Blur_Movie>().Radius);
        // while(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFilterPack_Blur_Movie>().Radius > 0)
        //  {
        //     GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFilterPack_Blur_Movie>().Radius -= 1;
        ///     yield return new WaitForSeconds(0.001f);
        // }
        yield return new WaitForSeconds(1f);
    }
    public void Fanzhuan()
    {
        PhotonView pv = GetComponent<PhotonView>();
        pv.RPC("SmokeStart", PhotonTargets.All);
        GameObject b0 = GameObject.FindGameObjectWithTag("Game0");
        GameObject b1 = GameObject.FindGameObjectWithTag("Game1");
        transform.GetComponent<NavMeshAgent>().enabled = false;
        GameObject w0 = GameObject.FindGameObjectWithTag("World0");
        GameObject w1 = GameObject.FindGameObjectWithTag("World1");
        
        
        if (GetComponent<PlayerINFO>().place == 0)
        {
            w0.GetComponent<AudioSource>().volume = 0;
            GameObject.Find("SunLight").transform.GetChild(0).gameObject.SetActive(true);
            b0.transform.GetChild(0).gameObject.SetActive(false);
            transform.position = transform.position + new Vector3(0, 100f, 0);
            b1.transform.GetChild(0).gameObject.SetActive(true);
            //b1.transform.GetChild(0).GetComponent<CameraFilterPack_Blur_Movie>().Radius = 0;
            //b1.transform.GetChild(0).GetComponent<CameraFilterPack_Blur_Movie>().Radius = 0;
            //StartCoroutine(CheckCamera());
            w1.GetComponent<AudioSource>().volume = 0.3f;
            if (GetComponent<PlayerINFO>().zhiYe == 2)
            {
                imageMouse = imageMouse3;
            }
        }
        else if (GetComponent<PlayerINFO>().place == 1)
        {
            w1.GetComponent<AudioSource>().volume = 0f;
            GameObject.Find("SunLight").transform.GetChild(0).gameObject.SetActive(false);
            b1.transform.GetChild(0).gameObject.SetActive(false);
            transform.position = transform.position + new Vector3(0, -100f, 0);
            b0.transform.GetChild(0).gameObject.SetActive(true);
            //b0.transform.GetChild(0).GetComponent<CameraFilterPack_Blur_Movie>().Radius = 1000;
            //b0.transform.GetChild(0).GetComponent<CameraFilterPack_Blur_Movie>().Radius = 0;
            //StartCoroutine(CheckCamera(b0.transform.GetChild(0)));
            w0.GetComponent<AudioSource>().volume = 0.3f;
            if (GetComponent<PlayerINFO>().zhiYe == 2)
            {
                imageMouse = imageMouse2;
            }
        }
        if (PlayerMSG.instance.PlayPro == "3")
        {
            GameObject.FindGameObjectWithTag("Shadow").transform.position = transform.position;
            GameObject.FindGameObjectWithTag("Shadow").transform.rotation = transform.rotation;
        }
        GetComponent<PlayerINFO>().place = 1 - GetComponent<PlayerINFO>().place;

        transform.GetComponent<NavMeshAgent>().enabled = true;
        transform.GetComponent<NavMeshAgent>().SetDestination(transform.position);
        MirrorFlipCamera.flipHorizontal = !MirrorFlipCamera.flipHorizontal;
    }
    IEnumerator CameraGo(Transform tr)
    {
        yield return new WaitForSeconds(1f);
        tr.gameObject.SetActive(true);
    }
    IEnumerator StopMove()
    {
        //Debug.LogError("xuanyun");
        isXY = true;
        PhotonView pv = transform.GetComponent<PhotonView>();
        pv.RPC("FaintAnim", PhotonTargets.All);
        yield return null;
        //yield return new WaitForSeconds(1f);
        transform.GetComponent<AnimManager>().WaitAnim();
        isXY = false;
        isMove = false;
        StartCoroutine(WaitCamera());//2018.8.29 2:00
    }
    IEnumerator WaitCamera()
    {
        GameObject.FindGameObjectWithTag("MainCamera").transform.DOLocalMove(new Vector3(0f, 15f, 10f), 1f);
        yield return new WaitForSeconds(1f);
        //GameObject.FindGameObjectWithTag("MainCamera").transform.localPosition += new Vector3(0, 7f, 5f);
        //GameObject.FindGameObjectWithTag("MainCamera").transform.DOLocalMove(new Vector3(0f, 15f, 10f), 1f);
    }
    void FreezeTime()
    {
        Time.timeScale = 0.1f;
        StartCoroutine(FreezeTimeController());
    }
    IEnumerator FreezeTimeController()
    {
        yield return new WaitForSeconds(0.01f);
        Time.timeScale = 1;
    }
    void ExplosionDamage(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            if ((hitColliders[i].tag == "Player" || hitColliders[i].tag =="col") && playerSelf.name != hitColliders[i].name)
            {
                navMeshAgent.SetDestination(transform.position - 8 * transform.forward);
                L3.Clear();
                StopCoroutine(OnMove());
                PhotonView pv = GetComponent<PhotonView>();
                pv.RPC("StopDashAnim", PhotonTargets.All);
                StartCoroutine(StopMove());
                break;
            }
            i++;
        }
    }
    IEnumerator Shas()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject.FindGameObjectWithTag("sha").transform.localScale = Vector3.zero;
    }
    void Update()
    {
        if (isMove)
        {
            ExplosionDamage(transform.position + new Vector3(0, 0.4f, 0), 0.3f);//判断碰撞
        }
        if (backTran > 1)
        {
            if (backTran == 3)
            {
                StopCoroutine(refreshBackTran());
            }
            new_y = Input.acceleration.y;
            currentDistance = new_y - old_y;
            old_y = new_y;
            if ((currentDistance > distance || Input.GetKeyDown(KeyCode.Space) || ETCInput.GetButtonDown("qiehuan")) && isBack == false)
            {
                if (backTran == 3)
                {
                    StartCoroutine(refreshBackTran());
                }
                backTran--;
                isBack = true;
                StartCoroutine(BackRotate());
                Fanzhuan();
            }
        }
        textBack.text = backTran.ToString();
        RaycastHit hinfo;
        GameObject hitObj = null;
        if (Physics.Linecast(mSwordHitablePoints[0].position, mSwordHitablePoints[1].position, out hinfo, mask2))
        {
            hitObj = hinfo.collider.gameObject;
            //Debug.LogError("error" + hitObj.name);           
        }
        else if (Physics.Linecast(mSwordHitablePoints[1].position, mSwordHitablePoints[2].position, out hinfo, mask2))
        {
            hitObj = hinfo.collider.gameObject;
            //Debug.LogError("error" + hitObj.name);
           
        }
        else if (Physics.Linecast(mSwordHitablePoints[2].position, mSwordHitablePoints[0].position, out hinfo, mask2))
        {
            hitObj = hinfo.collider.gameObject;
            //Debug.LogError("error" + hitObj.name);
        }
        else if (Physics.Linecast(mSwordHitablePoints[3].position, mSwordHitablePoints[4].position, out hinfo, mask2))
        {
            hitObj = hinfo.collider.gameObject;
            //Debug.LogError("error" + hitObj.name);
           
        }
        else if (Physics.Linecast(mSwordHitablePoints[4].position, mSwordHitablePoints[5].position, out hinfo, mask2))
        {
            hitObj = hinfo.collider.gameObject;
            //Debug.LogError("error" + hitObj.name);
            
        }
        else if (Physics.Linecast(mSwordHitablePoints[5].position, mSwordHitablePoints[3].position, out hinfo,mask2))
        {
            hitObj = hinfo.collider.gameObject;
            //Debug.LogError("error" + hitObj.name);
            
        }
        if(GetComponent<PlayerINFO>().zhiYe == 3)
        {
            if (Physics.Linecast(mSwordHitablePoints[6].position, mSwordHitablePoints[7].position, out hinfo))
            {
                hitObj = hinfo.collider.gameObject;
                //Debug.LogError("error" + hitObj.name);
            }
            else if (Physics.Linecast(mSwordHitablePoints[7].position, mSwordHitablePoints[8].position, out hinfo))
            {
                hitObj = hinfo.collider.gameObject;
               // Debug.LogError("error" + hitObj.name);
            }
            else if (Physics.Linecast(mSwordHitablePoints[8].position, mSwordHitablePoints[6].position, out hinfo))
            {
                hitObj = hinfo.collider.gameObject;
                //Debug.LogError("error" + hitObj.name);
            }
            else if (Physics.Linecast(mSwordHitablePoints[9].position, mSwordHitablePoints[10].position, out hinfo))
            {
                hitObj = hinfo.collider.gameObject;
                //Debug.LogError("error" + hitObj.name);
            }
            else if (Physics.Linecast(mSwordHitablePoints[10].position, mSwordHitablePoints[11].position, out hinfo))
            {
                hitObj = hinfo.collider.gameObject;
                //Debug.LogError("error" + hitObj.name);
            }
            else if (Physics.Linecast(mSwordHitablePoints[11].position, mSwordHitablePoints[9].position, out hinfo))
            {
                hitObj = hinfo.collider.gameObject;
                //Debug.LogError("error" + hitObj.name);
            }
        }
        if (hitObj != null && hitObj.tag == "Player" && hitObj.name != name && isMove == true 
            && hitObj.GetComponent<PlayerINFO>().team / (PlayerMSG.instance.maxPlayerNum / 2) != GetComponent<PlayerINFO>().team / (PlayerMSG.instance.maxPlayerNum / 2)
            && hitObj.GetComponent<PlayerINFO>().HP > -100)
        {
            //Handheld.Vibrate();//震动
            GameObject.FindGameObjectWithTag("MainCamera").transform.localPosition -= new Vector3(0, 9f, 7f);
            //FreezeTime();
            //Debug.Log("hitObj.name"+hitObj.name);
            PhotonView pv = hitObj.transform.GetComponent<PhotonView>();
            pv.RPC("HPDamage", PhotonTargets.All, 100);
            //Debug.LogError("error" + hitObj.name);
            if (hitObj.transform.GetComponent<PlayerINFO>().HP == 0 || hitObj.transform.GetComponent<PlayerINFO>().HP == -101)
            {       
                pv.RPC("Deathadd", PhotonTargets.All);
                GameObject.FindGameObjectWithTag("sha").transform.DOScale(3000, 0.5f);
                StartCoroutine(Shas());
                //pv.RPC("SetTargetPlayer", PhotonTargets.All,name);
                PhotonView pvSelf = transform.GetComponent<PhotonView>();
                pvSelf.RPC("Killadd", PhotonTargets.All, 1);
            }
            pv.RPC("PlayMusic", PhotonTargets.All, 1);
            pv.RPC("Penxue", PhotonTargets.All);
            StartCoroutine(WaitCamera());
            //GameObject.FindGameObjectWithTag("MainCamera").transform.localPosition += new Vector3(0, 10f, -8f);
        }
        else if (hitObj != null && hitObj.tag == "Weapon")
        {
            //Debug.LogError("pingdao");
            PhotonView pv = transform.GetComponent<PhotonView>();
            pv.RPC("PlayMusic", PhotonTargets.All, 0);
        }

        if (Input.GetMouseButton(0) && isMove == false && isXY == false && Input.mousePosition.x>151 && Input.mousePosition.y>151)
        {
            //L3.Clear();
            newMouse = Input.mousePosition;
            imageMouse.transform.position = newMouse;
            if ((newMouse - oldMouse).magnitude >= 1)
            {
                Vector3 rotationVector3 = new Vector3(0f, 0f, Vector2.SignedAngle(new Vector2(1, 0), newMouse - oldMouse) - 90f);
                imageMouse.transform.rotation = Quaternion.Euler(rotationVector3);
                oldMouse = newMouse;
            }
            //Debug.Log(Input.mousePosition);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, int.MaxValue, mask) && Input.mousePosition.x > 130 && Input.mousePosition.y > 130)
            {
                L3.Add(hit.point + new Vector3(0, 0, 0));
                if (PlayerMSG.instance.PlayPro == "3")
                     L4.Add(hit.point + new Vector3(0, 0, 0));
                if (L3.Count >= clickLength)
                {
                    //Debug.Log("out hit");
                    GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(0).gameObject.SetActive(false);
                    GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(1).gameObject.SetActive(true);
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && isMove == false && isXY == false)
        {
            imageMouse.transform.position = new Vector3(10000, 0, 0);
            StartCoroutine(OnMove());
        }
    }
    IEnumerator OnMove()
    {
        GetComponent<NavMeshAgent>().enabled = false;
        transform.DOMove(L3[0], 0.1f);
        //transform.position = L3[0];
        yield return new WaitForSeconds(0.001f);
        GetComponent<NavMeshAgent>().enabled = true;
        isMove = true;
        int len = L3.Count;
        if(len> clickLength)
        {
            len = clickLength;
        }

        /*navMeshAgent.SetDestination(L3[0]);
        while (navMeshAgent.path.corners.Length > 1)
        {
            yield return new WaitForSeconds(0.004f);
        }*/

        PhotonView pv = GetComponent<PhotonView>();
        
        if (len > 1) 
        {
            pv.RPC("StartDashAnim", PhotonTargets.All);
            //GetComponent<AnimManager>().StartDashAnim();//////////////////////////////////////////////
        }
        for (int i = 0; i < len - 1; i++)
        {
            if ((L3[i + 1] - L3[i]).magnitude <= 0.2f  && i != len - 2)
            {
                L3[i + 1] = L3[i];
                continue;
            }
            yield return new WaitForSeconds(0.01f);

            navMeshAgent.SetDestination(L3[i + 1]);
        }

        while (navMeshAgent.path.corners.Length > 1)
        {
            yield return new WaitForSeconds(0.01f);
        }
        if (len > 3)
        {
            pv.RPC("StopDashAnim", PhotonTargets.All);
            //GetComponent<AnimManager>().StopDashAnim();///////////////////////////////////////
        }
        StartCoroutine(Shanhou());
        if (PlayerMSG.instance.PlayPro == "3")
            StartCoroutine(OnMoveShadow());
        //GetComponent<AnimManager>().StopDashAnim();
    }
    IEnumerator Shanhou()
    {
        yield return 1;
        L3.Clear();
        GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(0).gameObject.SetActive(true);
        isMove = false;
    }
    IEnumerator OnMoveShadow()
    {
        GameObject sd = GameObject.FindGameObjectWithTag("Shadow");
        int len = L4.Count;
        if (len > clickLength)
        {
            len = clickLength;
        }
        for (int i = 0; i < len - 1; i++)
        {
            if ((L4[i + 1] - L4[i]).magnitude <= 0.2f && i != 0 && i != len - 2)
            {
                L4[i + 1] = L4[i];
                continue;
            }
            yield return new WaitForSeconds(0.005f);
            sd.transform.forward = L4[i + 1] - L4[i];
            sd.transform.DOMove(L4[i + 1], 0.005f);//.SetEase(Line);
        }
        L4.Clear();
        sd.transform.position = transform.position;
        sd.transform.rotation = transform.rotation;
    }
}
