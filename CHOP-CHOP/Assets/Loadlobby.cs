﻿using ExitGames.Client.Photon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loadlobby : Photon.PunBehaviour
{
    public Image progressImg;
    private AsyncOperation async;
    public Text text;

    private int curProgressVaule = 0;//计数器
    //private int maxNumPlayerPerRoom = 2;
    public Mutex mutex;
    private PhotonPeer peer;
    static public int ID = 0;
    string[] a;
    ExitGames.Client.Photon.Hashtable h;
    static public string room = "room";
    public Text context;
    public int nimPerson;
    // public string[] a = { "adam", "bill" };
    // Use this for initialization
    private void Awake()
    {
        PhotonNetwork.PhotonServerSettings.AppID = "00c931a2-d860-4946-95f2-8f80849356f5";
    }
    void Start()
    {

        //PhotonNetwork.ConnectUsingSettings("0.0.1");
        PhotonNetwork.ConnectToRegion(CloudRegionCode.cn, "0.0.8");
    }

    // Update is called once per frame
    void Update()
    {
        if (async == null)
        {
            return;
        }

        int progressVaule = 0;

        if (async.progress < 0.9f)
        {
            progressVaule = (int)async.progress * 100;
        }
        else
        {
            progressVaule = 100;
        }

        if (curProgressVaule < progressVaule)
        {
            curProgressVaule++;
        }
        text.text = curProgressVaule + "%";
        progressImg.fillAmount = curProgressVaule / 100f;
        if (curProgressVaule == 100)
        {
            async.allowSceneActivation = true;
        }
    }
    public void StartMatchingClick2()
    {
        PlayerMSG.instance.ActiveImage();
        //GameObject.FindGameObjectWithTag("Loading").SetActive(true);
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(false);

        PlayerMSG.instance.maxPlayerNum = 2;
        nimPerson = 2;
        PhotonNetwork.playerName = UnityEngine.Random.Range(1, 100000).ToString();
        //ID = ID + 1;
        //if(ID % maxNumPlayerPerRoom != 0)
        //{
        //    room = room + ID;
        //}
        //  PhotonNetwork.JoinOrCreateRoom(room, new RoomOptions { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
        //PhotonNetwork.JoinRoom(nimPerson+room+ID, a);
        PhotonNetwork.JoinRandomRoom(h, 2);
        
        //  PhotonNetwork.JoinRandomRoom();
    }


    public void StartMatchingClick4()
    {
        PlayerMSG.instance.ActiveImage();
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(false);

        nimPerson = 4;
        PhotonNetwork.playerName = UnityEngine.Random.Range(1, 100000).ToString();
        PlayerMSG.instance.maxPlayerNum = 4;
        //ID = ID + 1;
        //if(ID % maxNumPlayerPerRoom != 0)
        //{
        //    room = room + ID;
        //}
        //  PhotonNetwork.JoinOrCreateRoom(room, new RoomOptions { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
        PhotonNetwork.JoinRandomRoom(h, 4);

        //PhotonNetwork.JoinRandomRoom();
    }
    public void StartMatchingClick6()
    {
        PlayerMSG.instance.ActiveImage();
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(false);
        nimPerson = 6;
        PhotonNetwork.playerName = UnityEngine.Random.Range(1, 100000).ToString();
        PlayerMSG.instance.maxPlayerNum = 6;
        //ID = ID + 1;
        //if(ID % maxNumPlayerPerRoom != 0)
        //{
        //    room = room + ID;
        //}
        //  PhotonNetwork.JoinOrCreateRoom(room, new RoomOptions { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
        PhotonNetwork.JoinRandomRoom(h, 6);
        
        //PhotonNetwork.JoinRoom(p + r);
    }
    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        //       //PhotonNetwork.CreateRoom(Random.Range(1, 100000).ToString(), roomOptions, TypedLobby.Default);
        //int number = PhotonNetwork.playerList.Length;
        //Debug.Log("OnPhotonJoinRoomFailed numbers " + number + " room " + room);
        //if (number % maxNumPlayerPerRoom != 0)
        //{
        //    //           mutex.WaitOne();
        //    ID = ID + 1;
        //    room = room + ID;
        //    //           mutex.ReleaseMutex();
        //}
        //Debug.Log("OnPhotonJoinRoomFailed numbers " + number + " room " + room);
        //PhotonNetwork.CreateRoom(room, new RoomOptions() { MaxPlayers = Convert.ToByte(maxNumPlayerPerRoom) }, null);
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        int p = nimPerson;
        Debug.Log("nimPerson:" + nimPerson);
        ID = RandomNumber(1, 100000);
        PhotonNetwork.CreateRoom(p + room + ID, new RoomOptions() { MaxPlayers = Convert.ToByte(p) }, null);
        // PhotonNetwork.CreateRoom(p + room, new RoomOptions() { MaxPlayers = Convert.ToByte(p) }, null);
    }
    //public override void OnPhotonJoinFailed(object[] codeAndMsg)
    //{
    //    int p = nimPerson;
    //    int ID = RandomNumber(1, 100000);
    //    PhotonNetwork.CreateRoom(p + room + ID, new RoomOptions() { MaxPlayers = Convert.ToByte(p) }, null);
    //}
    public override void OnJoinedRoom()
    {
        //context.text = "waiting for match";
        Debug.Log("join room " + nimPerson + room);
        PhotonNetwork.automaticallySyncScene = true;
        //base.OnJoinedRoom();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        Debug.Log("OnPhotonPlayerConnected numbers" + nimPerson + "room" + room);
        //if (number % maxNumPlayerPerRoom != 0)
        //{
        //    //           mutex.WaitOne();
        //    ID = ID + 1;
        //    room = room + ID;
        //    //           mutex.ReleaseMutex();
        //}
        PlayerMSG.instance.teamId += 1;
        int maxNumPlayerPerRoom = nimPerson;
        int number = PhotonNetwork.playerList.Length;
        if (number % maxNumPlayerPerRoom == 0)
        {
            Debug.LogError(PlayerMSG.instance.teamId);
            //if (PhotonNetwork.isMasterClient)
            {
               // PhotonNetwork.LoadLevelAsync("Launcher");
                StartCoroutine(LoadScene());
                // AsyncLoadLevel("Launcher");//Async
            }
        }
    }
    IEnumerator LoadScene()
    {
        async = PhotonNetwork.LoadLevelAsync("Launcher");//异步跳转到game场景
        async.allowSceneActivation = false;//当game场景加载到90%时，不让它直接跳转到game场景。
        yield return async;
    }
    // Update is called once per frame
    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        base.OnPhotonPlayerDisconnected(otherPlayer);
        Destroy(this.gameObject);
    }
    public static int RandomNumber(int min, int max)
    {
        System.Random random = new System.Random();
        return random.Next(min, max);
    }

    public void OnDestroy()
    {
        Destroy(this.gameObject);
    }
}
