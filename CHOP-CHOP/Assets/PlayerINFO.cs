﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using DG.Tweening;
public class PlayerINFO : MonoBehaviour, IPunObservable
{
    public bool isDead = false;
    public int zhiYe = 0;
    public int place = 0;
    public int HP = 100;
    public int MP = 100;
    public int Kill = 0;
    public int Death = 0;
    public int realName = 65;
    int maxHP = 100, maxMP = 100;
    public Slider HPslider;
    public Slider MPslider;
    public Text realNameText;
    public int myname;
    public int team;//背面
    bool isDJS = false;
    public GameObject arr;
    public GameObject targetPlayer;
    bool isFirstGetinHere = true;
    void Start()
    {
        //team = PlayerMSG.instance.team;
        myname = Random.Range(10000, 99999);
        this.gameObject.name = myname.ToString();
        StartCoroutine(RefreshMP());
    }
    [PunRPC]
    void SetTargetPlayer(string gb)
    {
        targetPlayer = GameObject.Find(gb);
    }

    IEnumerator RefreshMP()
    {
        while (true)
        {
            if (MP < maxMP)
            {
                MP++;
            }
            yield return new WaitForSeconds(3f);
        }
    }
    void Update()
    {
        zhiYe = PlayerMSG.instance.zhiYe;
        HPslider.GetComponent<Slider>().value = (float)HP / maxHP;
        MPslider.GetComponent<Slider>().value = (float)MP / maxMP;
        realNameText.text = ((char)realName).ToString();
        
        
        // zhiYe = PlayerMSG.
        //Debug.Log(transform.rotation);
        // if (!photonView.isMine)
        //   {
        // transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
        //  transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        //  }
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(isDead);
            stream.SendNext(zhiYe);
            stream.SendNext(team);
            stream.SendNext(realName);
            stream.SendNext(this.HP);
            stream.SendNext(this.MP);
            stream.SendNext(this.myname);
            stream.SendNext(this.Kill);
            stream.SendNext(this.Death);
            //stream.SendNext(transform.position);
            //stream.SendNext(transform.rotation);
        }
        else
        {
            this.isDead = (bool)stream.ReceiveNext();
            this.zhiYe = (int)stream.ReceiveNext();
            this.team = (int)stream.ReceiveNext();
            realName = (int)stream.ReceiveNext();
            this.HP = (int)stream.ReceiveNext();
            this.MP = (int)stream.ReceiveNext();
            this.myname = (int)stream.ReceiveNext();
            this.Kill = (int)stream.ReceiveNext();
            this.Death = (int)stream.ReceiveNext();


            this.gameObject.name = myname.ToString();
            //correctPlayerPos = (Vector3)stream.ReceiveNext();
            //correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }
    //private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    // private Quaternion correctPlayerRot = Quaternion.identity;
    [PunRPC]
    public void HPDamage(int i)
    {
        HP -= i;
        if (HP <= 0)
        {
            if (this.gameObject == GameObject.FindGameObjectWithTag("Player"))
            {
                GameObject.FindGameObjectWithTag("Game0").transform.GetChild(0).GetComponent<KillSelf>().BloodDamage(i, HP);
            }
            isDead = true;
            GetComponent<AnimManager>().DeathAnim();
            StartCoroutine(Refresh());
        }
        else if (i > 0)
        {
            if (this.gameObject == GameObject.FindGameObjectWithTag("Player"))
                GameObject.FindGameObjectWithTag("Game0").transform.GetChild(0).GetComponent<KillSelf>().BloodDamage(i, HP);
            GetComponent<AnimManager>().DamageAnim();
        }
    }
    public void MPDamage(int i)
    {
        MP -= i;
    }
    void HPMPRefresh()
    {
        if (HP <= 0)
        {
            HP = maxHP;
            MP = maxMP;
        }
        if (this.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            GameObject.FindGameObjectWithTag("Game0").transform.GetChild(0).GetComponent<KillSelf>().BloodRef();
        }
    }
    [PunRPC]
    void Killadd(int i)
    {
        Kill += i;
    }
    [PunRPC]
    void SetTeam(int i)
    {
        team = i;
    }
    [PunRPC]
    void Deathadd()
    {
        Death++;
    }
    IEnumerator Daojishi()
    {
        FreezeTime();
        isDJS = true;
        yield return new WaitForSeconds(3f);
        //Debug.Log(123333);
        /*GameObject Djs = GameObject.FindGameObjectWithTag("Respawn");
        for (int i = 3; i > 0; i--)
        {
            Djs.transform.DOScale(8f, 0.0001f);
            Djs.GetComponent<Text>().text = i.ToString();
            Djs.transform.DOScale(0.1f, 0.8f);
            yield return new WaitForSeconds(0.8f);
            Djs.transform.DOScale(8f, 0.0001f);
        }
        Djs.GetComponent<Text>().text = "";*/
        isDJS = false;
    }
    void FreezeTime()
    {
        Time.timeScale = 0.1f;
        StartCoroutine(FreezeTimeController());
    }
    IEnumerator FreezeTimeController()
    {
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 0.4f;
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 3;
        yield return new WaitForSeconds(3f);
        Time.timeScale = 1;
    }
    IEnumerator Refresh()
    {
        yield return new WaitForSeconds(0.15f);
        GameObject.FindGameObjectWithTag("MainCamera").transform.DOShakePosition(1,2);
        bool swit = false;
        
        //yield return new WaitForSeconds(1);
        if (GetComponent<LineManager>().enabled == true)
        {
            GetComponent<LineManager>().enabled = false;
            //GetComponent<AnimManager>().enabled = false;
            swit = true;
            //Debug.Log(GetComponent<LineManager>().enabled  + name + "im here");
        }
        if (this.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            if (!isDJS)
            {
                StartCoroutine(Daojishi());
                yield return new WaitForSeconds(3f);
                gameObject.GetComponent<LineManager>().Fanzhuan();
                if (PlayerMSG.instance.PlayPro == "3")
                {
                    GameObject.FindGameObjectWithTag("Shadow").transform.position = transform.position;
                    GameObject.FindGameObjectWithTag("Shadow").transform.rotation = transform.rotation;
                }
            }
        }
        else
        {
            yield return new WaitForSeconds(3f);
        }
        if (isFirstGetinHere)
        {
            StartCoroutine(SetEnableLoad()); 
        }
        // if (photonView.isMine)
        // {
        if (swit)
        {
            GetComponent<LineManager>().enabled = true;
            //GetComponent<AnimManager>().enabled = true;
        }
        //}
        isDead = false;
        //yield return new WaitForSeconds(3f);///////2018.8.26
        GetComponent<AnimManager>().WaitAnim();
        HPMPRefresh();
    }
    IEnumerator SetEnableLoad()
    {
        yield return new WaitForSeconds(1f);
        PlayerMSG.instance.UnactiveImage();
        isFirstGetinHere = false;
    }
}
