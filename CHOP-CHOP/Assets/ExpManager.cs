﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class ExpManager : MonoBehaviour
{
    public GameObject[] playerList;
    public GameObject playerSelf;
    //public List<GameObject> aa;
    public List<int> killList;
    public List<int> DeathList;
    bool isLoadPlayer = false;
    public Text timeClock;
    public GameObject result;
    public bool isEnd;
    public Text team0;
    public Text team1;
    public int teamSelf;
    private int maxKill = 40;
    public Text Kda;
    public GameObject k0;
    public GameObject k1;

    // Use this for initialization
    void Start()
    {
        playerSelf = GameObject.FindGameObjectWithTag("Player");
        teamSelf = playerSelf.GetComponent<PlayerINFO>().team;
        StartCoroutine(findplayer());
    }
    IEnumerator findplayer()
    {
        yield return new WaitForSeconds(3f);
        playerSelf = GameObject.FindGameObjectWithTag("Player");
        playerList = GameObject.FindGameObjectsWithTag("Player");
        maxKill = PlayerMSG.instance.maxPlayerNum * 4 - 4;
        timeClock.text = "TargetKill: " + maxKill;
        LoadPlayerData();
        /*int timei = 183;
        while (timei > 0)
        {
            if(timei == 180)
            {
                GameObject Djs = GameObject.FindGameObjectWithTag("Respawn");
                Djs.GetComponent<Text>().text = "GameStart";
                Djs.transform.DOScale(0.001f, 0.0001f);
                //Djs.SetActive(false);
            }
            timei--;
            timeClock.text = "Time: " + timei / 60 + ":" + timei % 60;
            yield return new WaitForSeconds(1);
        }*/
    }
    IEnumerator Shas()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject.FindGameObjectWithTag("sha2").SetActive(false);
    }
    void LoadPlayerData()
    {
        int length = playerList.Length;
        for (int i = 0; i < length; i++)
        {
            if (playerList[i].GetComponent<PlayerINFO>().team == teamSelf)
            {
                playerList[i].GetComponent<HighlightableObject>().ConstantOn(Color.white);
                while (playerList[i].transform.Find("Weapon"))
                {
                    playerList[i].transform.Find("Weapon/TrailTeam").gameObject.SetActive(false);
                    playerList[i].transform.Find("Weapon/TrailEnemy").gameObject.SetActive(false);
                    playerList[i].transform.Find("Weapon").name = "W";
                }
            }
            else if(playerList[i].GetComponent<PlayerINFO>().team / (PlayerMSG.instance.maxPlayerNum/2) == teamSelf / (PlayerMSG.instance.maxPlayerNum / 2))
            {
                playerList[i].GetComponent<HighlightableObject>().ConstantOn(new Color(0,178f/255f,1));
                while (playerList[i].transform.Find("Weapon"))
                {
                    playerList[i].transform.Find("Weapon/TrailSelf").gameObject.SetActive(false);
                    playerList[i].transform.Find("Weapon/TrailEnemy").gameObject.SetActive(false);
                    playerList[i].transform.Find("Weapon").name = "W";
                }
            }
            else
            {
                playerList[i].GetComponent<HighlightableObject>().ConstantOn(Color.red);
                while (playerList[i].transform.Find("Weapon"))
                {
                    playerList[i].transform.Find("Weapon/TrailSelf").gameObject.SetActive(false);
                    playerList[i].transform.Find("Weapon/TrailTeam").gameObject.SetActive(false);
                    playerList[i].transform.Find("Weapon").name = "W";
                }
            }
            //GameObject kk = Resources.Load("head") as GameObject;
           // aa.Add(Instantiate(kk, transform) as GameObject);

            killList.Add(0);
            DeathList.Add(0);
        }
        isLoadPlayer = true;
        /*for (int i = 0; i < length; i++)
        {
            if (playerList[i].name == playerSelf.name)
            {
                aa[i].transform.GetChild(1).gameObject.SetActive(true);
            }
        }*/
    }
    // Update is called once per frame
    void Update()
    {
        if (playerSelf == null)
        {
            playerSelf = GameObject.FindGameObjectWithTag("Player");
            //Debug.Log(playerSelf.name);
        }
        if (playerList.Length < PlayerMSG.instance.maxPlayerNum)
        {
            playerList = GameObject.FindGameObjectsWithTag("Player");
            int length = playerList.Length;
            killList.Add(0);
            DeathList.Add(0);

            for (int i = length-1; i < length; i++)
            {
                if (playerList[i].GetComponent<PlayerINFO>().team == teamSelf)
                {
                    playerList[i].GetComponent<HighlightableObject>().ConstantOn(Color.white);
                    while (playerList[i].transform.Find("Weapon"))
                    {
                        playerList[i].transform.Find("Weapon/TrailTeam").gameObject.SetActive(false);
                        playerList[i].transform.Find("Weapon/TrailEnemy").gameObject.SetActive(false);
                        playerList[i].transform.Find("Weapon").name = "W";
                    }
                }
                else if (playerList[i].GetComponent<PlayerINFO>().team / (PlayerMSG.instance.maxPlayerNum / 2) == teamSelf / (PlayerMSG.instance.maxPlayerNum / 2))
                {
                    playerList[i].GetComponent<HighlightableObject>().ConstantOn(new Color(0, 178f / 255f, 1));
                    while (playerList[i].transform.Find("Weapon"))
                    {
                        playerList[i].transform.Find("Weapon/TrailSelf").gameObject.SetActive(false);
                        playerList[i].transform.Find("Weapon/TrailEnemy").gameObject.SetActive(false);
                        playerList[i].transform.Find("Weapon").name = "W";
                    }
                }
                else
                {
                    playerList[i].GetComponent<HighlightableObject>().ConstantOn(Color.red);
                    while (playerList[i].transform.Find("Weapon"))
                    {
                        playerList[i].transform.Find("Weapon/TrailSelf").gameObject.SetActive(false);
                        playerList[i].transform.Find("Weapon/TrailTeam").gameObject.SetActive(false);
                        playerList[i].transform.Find("Weapon").name = "W";
                    }
                }
            }
            isLoadPlayer = true;
        }
        if (isLoadPlayer == true)
        {
            if (playerSelf.GetComponent<PlayerINFO>().team >= PlayerMSG.instance.maxPlayerNum / 2) 
            {
                k1.SetActive(true);
            }
            else
            {
                k0.SetActive(true);
            }
            Kda.text = "            " + playerSelf.GetComponent<PlayerINFO>().Kill + "             " + playerSelf.GetComponent<PlayerINFO>().Death;
            for (int i = 0; i < playerList.Length; i++)
            {
                killList[i] = playerList[i].GetComponent<PlayerINFO>().Kill;
                DeathList[i] = playerList[i].GetComponent<PlayerINFO>().Death;
                
                //aa[i].transform.GetChild(0).GetComponent<Text>().text = "Kill" + (killList[i]);
            }
            {
                int t0 = 0;
                int t1 = 0;
                for (int i = 0; i < playerList.Length; i++)
                {
                    if (playerList[i].GetComponent<PlayerINFO>().team >= PlayerMSG.instance.maxPlayerNum / 2)
                    {
                        t0 += playerList[i].GetComponent<PlayerINFO>().Death;
                    }
                    else
                    {
                        t1 += playerList[i].GetComponent<PlayerINFO>().Death;
                    }
                    if(t0 >=maxKill || t1>= maxKill)
                    {
                        isEnd = true;
                    }
                }
                team0.text = t0.ToString();
                team1.text = t1.ToString();
            }
        }

        if (isEnd == true)
        {
            if (int.Parse(team0.text) > int.Parse(team1.text))
            {
                if (playerList[0].transform.GetComponent<PlayerINFO>().team < PlayerMSG.instance.maxPlayerNum / 2)
                {
                    result.transform.GetChild(0).gameObject.SetActive(true);
                    GameObject.FindGameObjectWithTag("sha2").transform.DOScale(3000, 0.5f);
                    StartCoroutine(Shas());
                    //result.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "YOU WIN";
                    for (int i = 0, j = 0; i < playerList.Length; i++)
                    {
                        if (playerList[i].transform.GetComponent<PlayerINFO>().team < PlayerMSG.instance.maxPlayerNum / 2)
                        {
                            result.transform.GetChild(0).GetChild(j).gameObject.SetActive(true);
                            result.transform.GetChild(0).GetChild(j).GetComponent<Text>().text = " " + playerList[i].transform.GetComponent<PlayerINFO>().Kill + " - " + playerList[i].transform.GetComponent<PlayerINFO>().Death;
                            if (playerList[i].transform.GetComponent<PlayerINFO>().team == playerSelf.GetComponent<PlayerINFO>().team)
                            {
                                result.transform.GetChild(0).GetChild(j).GetComponent<Text>().text += "YourSelf";
                            }
                            else
                            {
                                result.transform.GetChild(0).GetChild(j).GetComponent<Text>().text += "TeamMate";
                            }
                            j++;
                        }
                    }
                }
                else
                {
                    result.transform.GetChild(1).gameObject.SetActive(true);
                    //result.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "YOU LOSE";
                    for (int i = 0, j = 0; i < playerList.Length; i++)
                    {
                        if (playerList[i].transform.GetComponent<PlayerINFO>().team >= PlayerMSG.instance.maxPlayerNum / 2)
                        {
                            result.transform.GetChild(1).GetChild(j).gameObject.SetActive(true);
                            result.transform.GetChild(1).GetChild(j).GetComponent<Text>().text = " " + playerList[i].transform.GetComponent<PlayerINFO>().Kill + " - " + playerList[i].transform.GetComponent<PlayerINFO>().Death;
                            if (playerList[i].transform.GetComponent<PlayerINFO>().team == playerSelf.GetComponent<PlayerINFO>().team)
                            {
                                result.transform.GetChild(1).GetChild(j).GetComponent<Text>().text += "YourSelf";
                            }
                            else
                            {
                                result.transform.GetChild(1).GetChild(j).GetComponent<Text>().text += "YourTeamMate";
                            }
                            j++;
                        }
                    }
                }
            }
            else if (int.Parse(team0.text) < int.Parse(team1.text))
            {
                if (playerList[0].transform.GetComponent<PlayerINFO>().team < PlayerMSG.instance.maxPlayerNum / 2)
                {
                    //result.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "YOU LOSE";
                    result.transform.GetChild(1).gameObject.SetActive(true);
                    for (int i = 0, j = 0; i < playerList.Length; i++)
                    {
                        if (playerList[i].transform.GetComponent<PlayerINFO>().team < PlayerMSG.instance.maxPlayerNum / 2)
                        {
                            result.transform.GetChild(1).GetChild(j).gameObject.SetActive(true);
                            result.transform.GetChild(1).GetChild(j).GetComponent<Text>().text = " " + playerList[i].transform.GetComponent<PlayerINFO>().Kill + " - " + playerList[i].transform.GetComponent<PlayerINFO>().Death;
                            if (playerList[i].transform.GetComponent<PlayerINFO>().team == playerSelf.GetComponent<PlayerINFO>().team)
                            {
                                result.transform.GetChild(1).GetChild(j).GetComponent<Text>().text += "YourSelf";
                            }
                            else
                            {
                                result.transform.GetChild(1).GetChild(j).GetComponent<Text>().text += "YourTeamMate";
                            }
                            j++;
                        }
                    }
                }
                else
                {
                    result.transform.GetChild(0).gameObject.SetActive(true);
                    GameObject.FindGameObjectWithTag("sha2").transform.DOScale(3000, 0.5f);
                    StartCoroutine(Shas());
                    //result.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "YOU WIN";
                    for (int i = 0, j = 0; i < playerList.Length; i++)
                    {
                        if (playerList[i].transform.GetComponent<PlayerINFO>().team >= PlayerMSG.instance.maxPlayerNum / 2)
                        {
                            result.transform.GetChild(0).GetChild(j).gameObject.SetActive(true);
                            result.transform.GetChild(0).GetChild(j).GetComponent<Text>().text = " " + playerList[i].transform.GetComponent<PlayerINFO>().Kill + " - " + playerList[i].transform.GetComponent<PlayerINFO>().Death;
                            if (playerList[i].transform.GetComponent<PlayerINFO>().team == playerSelf.GetComponent<PlayerINFO>().team)
                            {
                                result.transform.GetChild(0).GetChild(j).GetComponent<Text>().text += "YourSelf";
                            }
                            else
                            {
                                result.transform.GetChild(0).GetChild(j).GetComponent<Text>().text += "YourTeamMate";
                            }
                            j++;
                        }
                    }
                }
            }
            else
            {
                result.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "NO ONE WIN";
            }
            result.SetActive(true);
            isEnd = false;
            PhotonNetwork.LeaveRoom();
            Time.timeScale = 1;
        }
    }
}
