﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public List<AudioClip> Sound;
	// Use this for initialization
	void Start () {
        Sound.Add((AudioClip)Resources.Load("AudioSource/PingDao", typeof(AudioClip)));
        Sound.Add((AudioClip)Resources.Load("AudioSource/ShouJi", typeof(AudioClip)));
        Sound.Add((AudioClip)Resources.Load("AudioSource/Badao", typeof(AudioClip)));
        Sound.Add((AudioClip)Resources.Load("AudioSource/Badao1", typeof(AudioClip)));
        Sound.Add((AudioClip)Resources.Load("AudioSource/Jingzi", typeof(AudioClip)));
        Sound.Add((AudioClip)Resources.Load("AudioSource/Fork", typeof(AudioClip)));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    [PunRPC]
    public void PlayMusic(int type)
    {
        GetComponent<AudioSource>().clip = Sound[type];
        GetComponent<AudioSource>().Play();
    }

}
