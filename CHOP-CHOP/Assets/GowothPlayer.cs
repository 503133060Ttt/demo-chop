﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class GowothPlayer : MonoBehaviour {
    GameObject m_Player;
    public Vector3 offset;
	// Use this for initialization
	void Start () {
        while (true)
        {
            m_Player = GameObject.FindWithTag("Player");
            if (m_Player != null)
                break;
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (m_Player.transform.GetComponent<LineManager>().isMove == false)
        {
            //   transform.position = m_Player.transform.position + offset;
            transform.DOMoveX(m_Player.transform.position.x*1, 1f);
            transform.DOMoveZ(m_Player.transform.position.z*1, 1f);

            transform.GetChild(0).DOLocalMove(new Vector3(0, 15, 10),1f);
        }
    }
}
