﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillSelf : MonoBehaviour {
    public int HP = 100;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    
    public void BloodDamage(int iBlood,int HP)
    {
        BleedBehavior.BloodAmount += Mathf.Clamp01(0.75f);
        BleedBehavior.minBloodAmount += (float)0.50f;
    }
    public void BloodRef()
    {
        BleedBehavior.minBloodAmount = 0;
        BleedBehavior.BloodAmount = 0;
    }
}
