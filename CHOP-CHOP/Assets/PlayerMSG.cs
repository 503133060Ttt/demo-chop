﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMSG : MonoBehaviour {
    static public PlayerMSG instance;
    string PlayerName = "ZXT";
    public string PlayPro = "9";
    public int teamId = 0;
    public int maxPlayerNum = 0;
    public int zhiYe = 0;
    public Image loading;
    // Use this for initialization
    private void Awake()
    {
        Application.targetFrameRate = 60;
    }
    void Start () {
        instance = this;
        Application.targetFrameRate = 60;
        GameObject.DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ActiveImage()
    {
        loading.gameObject.SetActive(true);
    }
    public void UnactiveImage()
    {
        loading.gameObject.SetActive(false);
    }
}
