﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Page_Menu : MonoBehaviour
{

    public Button btn_logo;
    public Button btn_play1;

    public Transform menuPos_1;
    public Transform menuPos_2;


    public Image[] Character;

    public GameObject[] playBtn;

    public GameObject[] btn = new GameObject[4];

    public void Start()
    {
        btn_logo.onClick.AddListener(OnClickMenu);
        btn_play1.onClick.AddListener(PlayGame);
        menuPos_2.gameObject.SetActive(false);
        btn_logo.gameObject.transform.position = menuPos_1.transform.position;
    }

    public void OnClickMenu()
    {
        btn_logo.gameObject.transform.DOMove(menuPos_2.transform.position - new Vector3(1000f,0,0), 0.6f);
        menuPos_2.gameObject.SetActive(true);
        //Debug.Log("add");
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
    }

    public void PlayGame()
    {
        menuPos_2.gameObject.SetActive(false);
        btn_logo.gameObject.transform.DOMove(menuPos_1.transform.position, 0.3f);
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
    }
    public void OnClickCharacter0()
    {
        btn[0].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "0";
        PlayerMSG.instance.zhiYe = 0;
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        Character[0].gameObject.SetActive(true);
        //Character[0].transform.DOShakeScale(0.5f,0.5f);
        Character[1].gameObject.SetActive(false);
        Character[2].gameObject.SetActive(false);
        Character[3].gameObject.SetActive(false);
    }
    public void OnClickCharacter1()
    {
        btn[1].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "1";
        PlayerMSG.instance.zhiYe = 1;
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        Character[0].gameObject.SetActive(false);
        Character[1].gameObject.SetActive(true);
        //Character[1].transform.DOShakeScale(0.5f, 0.5f);
        Character[2].gameObject.SetActive(false);
        Character[3].gameObject.SetActive(false);
    }
    public void OnClickCharacter2()
    {
        btn[2].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "2";
        PlayerMSG.instance.zhiYe = 2;
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        Character[0].gameObject.SetActive(false);
        Character[1].gameObject.SetActive(false);
        Character[2].gameObject.SetActive(true);
        //Character[2].transform.DOShakeScale(0.5f, 0.5f);
        Character[3].gameObject.SetActive(false);
    }
    public void OnClickCharacter3()
    {
        btn[3].transform.SetAsLastSibling();
        PlayerMSG.instance.PlayPro = "3";
        PlayerMSG.instance.zhiYe = 3;
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();
        Character[0].gameObject.SetActive(false);
        Character[1].gameObject.SetActive(false);
        Character[2].gameObject.SetActive(false);
        Character[3].gameObject.SetActive(true);
        //Character[3].transform.DOShakeScale(0.5f, 0.5f);
    }

    public void OnClickShowPlay()
    {
        //btn[3].transform.SetAsLastSibling();
        
        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();

        playBtn[0].SetActive(true);
        playBtn[1].SetActive(true);

    }
    public void OnClickReturnPlay()
    {
        //btn[3].transform.SetAsLastSibling();

        GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();

        playBtn[0].SetActive(false);
        playBtn[1].SetActive(false);

    }
}
