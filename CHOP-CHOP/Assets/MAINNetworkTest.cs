﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class MAINNetworkTest : MonoBehaviour
{
    static public MAINNetworkTest instance;
    public GameObject player0;
    public GameObject player1;
    public GameObject player2;
    public GameObject fork;
    public GameObject Shadow;
    public Image startImage; 
    // Use this for initialization
    void Start()
    {
        instance = this;
        StartCoroutine(LoadGame());
    }
    IEnumerator LoadGame()
    {
        yield return new WaitForSeconds(4.5f);
        startImage.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator StartPlay()
    {
        yield return null;
        PhotonView pv3 = GameObject.FindGameObjectWithTag("Player").GetComponent<PhotonView>();
        pv3.RPC("HPDamage", PhotonTargets.All, 101);
    }
    void loadBackPlayer()
    {
        GameObject.FindGameObjectWithTag("Game0").transform.GetChild(0).gameObject.SetActive(false);
        GameObject.FindGameObjectWithTag("Game1").transform.GetChild(0).gameObject.SetActive(true);
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerINFO>().place = 1;
        GameObject.FindGameObjectWithTag("Player").GetComponent<NavMeshAgent>().SetDestination(this.transform.position);
        //GameObject.FindGameObjectWithTag("Mouse").transform.Rotate(0, 180, 0);
        MirrorFlipCamera.flipHorizontal = !MirrorFlipCamera.flipHorizontal;
    }
    private void OnLevelWasLoaded(int level)
    {
        if (PlayerMSG.instance.PlayPro == "0")
        {
            if (PlayerMSG.instance.teamId < PlayerMSG.instance.maxPlayerNum / 2)
            {
                PhotonNetwork.Instantiate(fork.name, new Vector3(Random.Range(-4, 4), 0f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                GameObject.Find("SunLight").transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                PhotonNetwork.Instantiate(fork.name, new Vector3(Random.Range(-2, 2), 100f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                loadBackPlayer();
            }

            StartCoroutine(StartPlay());
        }
        if (PlayerMSG.instance.PlayPro == "1")
        {
            if (PlayerMSG.instance.teamId < PlayerMSG.instance.maxPlayerNum / 2)
            {
                PhotonNetwork.Instantiate(player2.name, new Vector3(Random.Range(-5, 5), 0f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                GameObject.Find("SunLight").transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                PhotonNetwork.Instantiate(player2.name, new Vector3(Random.Range(-5, 5), 100f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                loadBackPlayer();
            }
            StartCoroutine(StartPlay());
        }
        if (PlayerMSG.instance.PlayPro == "2")
        {
            if (PlayerMSG.instance.teamId < PlayerMSG.instance.maxPlayerNum / 2)
            {
                PhotonNetwork.Instantiate(player0.name, new Vector3(Random.Range(-5, 3), 0f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                GameObject.Find("SunLight").transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                PhotonNetwork.Instantiate(player0.name, new Vector3(Random.Range(-4, 5), 100f, Random.Range(-2,2)), new Quaternion(0, 0, 0, 0), 0);
                loadBackPlayer();
            }
            StartCoroutine(StartPlay());
        }
        else if (PlayerMSG.instance.PlayPro == "3")
        {
            if (PlayerMSG.instance.teamId < PlayerMSG.instance.maxPlayerNum / 2)
            {
                GameObject kk = PhotonNetwork.Instantiate(player1.name, new Vector3(Random.Range(-2, 2), 0f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                GameObject kk2 = PhotonNetwork.Instantiate(Shadow.name, kk.transform.position, new Quaternion(0, 0, 0, 0), 0);
                kk.GetComponent<LineManager>().mSwordHitablePoints[6] = kk2.transform.GetChild(2).transform.GetChild(1).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[7] = kk2.transform.GetChild(2).transform.GetChild(2).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[8] = kk2.transform.GetChild(2).transform.GetChild(3).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[9] = kk2.transform.GetChild(2).transform.GetChild(4).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[10] = kk2.transform.GetChild(2).transform.GetChild(5).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[11] = kk2.transform.GetChild(2).transform.GetChild(6).transform;
                GameObject.Find("SunLight").transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                GameObject kk = PhotonNetwork.Instantiate(player1.name, new Vector3(Random.Range(-4, 4), 100f, Random.Range(-2, 2)), new Quaternion(0, 0, 0, 0), 0);
                GameObject kk2 = PhotonNetwork.Instantiate(Shadow.name, kk.transform.position, new Quaternion(0, 0, 0, 0), 0);
                kk.GetComponent<LineManager>().mSwordHitablePoints[6] = kk2.transform.GetChild(2).transform.GetChild(1).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[7] = kk2.transform.GetChild(2).transform.GetChild(2).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[8] = kk2.transform.GetChild(2).transform.GetChild(3).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[9] = kk2.transform.GetChild(2).transform.GetChild(4).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[10] = kk2.transform.GetChild(2).transform.GetChild(5).transform;
                kk.GetComponent<LineManager>().mSwordHitablePoints[11] = kk2.transform.GetChild(2).transform.GetChild(6).transform;
                loadBackPlayer();
            }
            StartCoroutine(StartPlay());
        }
        //Debug.LogError(PlayerMSG.instance.teamId);
        PhotonView pv = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<PhotonView>();
        pv.RPC("SetTeam", PhotonTargets.All, PlayerMSG.instance.teamId);


        //GameObject.FindGameObjectWithTag("Player").transform.SetParent(GameObject.FindGameObjectWithTag("ZDown").transform);
    }
}
