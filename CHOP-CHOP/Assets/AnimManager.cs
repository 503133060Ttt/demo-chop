﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class AnimManager : MonoBehaviour
{
    Animator anim;
    public GameObject[] weapon;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        //Debug.Log("start"+ anim);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [PunRPC]
    public void StopDashAnim()
    {
        anim.Play("StopDash", 0);
        if (weapon.Length == 1)
        {
            weapon[0].transform.DOScale(100f, 0.2f);
            transform.GetChild(4).DOScale(100f, 0.2f);
        }
        else if (weapon.Length == 2)
        {
            //weapon[0].transform.RotateAround(weapon[0].transform.GetChild(0).position, new Vector3(0, 1, 0), 90);



            weapon[0].transform.DOLocalMove(new Vector3(2.4f, 0.2f, 2.6f),0.01f);
            weapon[0].transform.localRotation = new Quaternion(0.0f, 0.4f, 0.0f, 0.9f);

            //weapon[1].transform.RotateAround(weapon[1].transform.GetChild(0).position, new Vector3(0, -1, 0), 90);

            weapon[1].transform.DOLocalMove(new Vector3(-2.9f, 0.4f, 3.0f), 0.01f);
            weapon[1].transform.localRotation = new Quaternion(-0.1f, -0.5f, 0.0f, 0.9f);
        }
        else if (weapon.Length == 3)
        {
            //weapon[0].transform.RotateAround(weapon[0].transform.GetChild(0).position, new Vector3(0, -1, 0), 90);
            weapon[0].transform.localRotation = new Quaternion(0.0f, -1.0f, 0.0f, 0.1f);
            weapon[0].transform.localPosition = new Vector3(-2.4f, 2.0f, -4.4f);
        }
        else if (weapon.Length == 4)
        {
            if (name == GameObject.FindGameObjectWithTag("Player").name)
            {
                GetComponent<AudioManager>().PlayMusic(5);
            }
            for (int i = 0; i < 4; i++)
            {
                weapon[i].transform.localPosition = new Vector3(0f, 1.5f, 5f);
            }
        }
    }
    [PunRPC]
    public void WaitAnim()
    {
        anim.Play("Wait", 0);
    }
    public void DeathAnim()
    {
       anim.Play("Dead", 0);
    }
    public void DamageAnim()
    {
        anim.Play("Damage", 0);
    }
    [PunRPC]
    public void FaintAnim()
    {
        anim.Play("Faint", 0);
    }
    [PunRPC]
    public void StartDashAnim()
    {
        anim.Play("StartDash", 0);
        if (weapon.Length == 1)
        {
            weapon[0].transform.DOScale(300f, 0.000001f);
            transform.GetChild(4).DOScale(300f, 0.000001f);
            if (name == GameObject.FindGameObjectWithTag("Player").name)
            {
                GetComponent<AudioManager>().PlayMusic(4);
            }
        }
        else if (weapon.Length == 2)
        {
            weapon[0].transform.RotateAround(weapon[0].transform.GetChild(0).position, new Vector3(0, -1, 0), 90);
            weapon[1].transform.RotateAround(weapon[1].transform.GetChild(0).position, new Vector3(0, 1, 0), 90);
            if (name == GameObject.FindGameObjectWithTag("Player").name)
            {
                GetComponent<AudioManager>().PlayMusic(2);
            }
        }
        else if (weapon.Length == 3)
        {
            weapon[0].transform.RotateAround(weapon[0].transform.GetChild(0).position, new Vector3(0, 1, 0), 90);
            if (name == GameObject.FindGameObjectWithTag("Player").name)
            {
                GetComponent<AudioManager>().PlayMusic(3);
            }
        }
        else if(weapon.Length == 4)
        {
            weapon[0].transform.DOMove(weapon[0].transform.position + transform.right * 2, 0.0001f);
            weapon[1].transform.DOMove(weapon[1].transform.position + transform.right , 0.0001f);
            weapon[2].transform.DOMove(weapon[2].transform.position - transform.right , 0.0001f);
            weapon[3].transform.DOMove(weapon[3].transform.position - transform.right * 2, 0.0001f);
        }
    }
    
    IEnumerator Shoudao1(Transform hand)
    {
        for (int i = 0; i < 4; i++)
        {
            weapon[i].transform.DOLocalMove(hand.GetChild(0).position, 0.001f);
        }
        yield return new WaitForSeconds(0.001f);
    }
    [PunRPC]
    public void Penxue()
    {
        GameObject px = transform.GetChild(0).gameObject;
        px.transform.position = transform.position;
        StartCoroutine(Liuxue(px));
    }
    IEnumerator Liuxue(GameObject px)
    {
        yield return new WaitForSeconds(2f);
        px.transform.position = new Vector3(0, 1000, 0);
    }
}
